<?php


namespace KDA\Laravel\Actions\Contracts;

trait HasFilename
{
    protected string $filename;


    public function filename(string $filename): static
    {
        $this->filename = $filename;
        return $this;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

}
