<?php


namespace KDA\Laravel\Actions\Contracts;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\FilesystemAdapter;
use Symfony\Component\HttpFoundation\StreamedResponse;

trait HasDisk
{

    protected string $disk;
    
    public function disk(string $disk): static
    {
        $this->disk = $disk;
        return $this;
    }

    public function getDisk(): string
    {
        return $this->disk;
    }

    public function getStorage(): FilesystemAdapter
    {
        return Storage::disk($this->getDisk());
    }

    public function download(): StreamedResponse
    {
        return $this->getStorage()->download($this->getFilename());
    }

    public function store(mixed $content):static{
        Storage::disk($this->getDisk())->put($this->getFilename() , $content);
        return $this;
    }
}
