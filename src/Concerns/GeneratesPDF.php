<?php


namespace KDA\Laravel\Actions\Contracts;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\FilesystemAdapter;
use Symfony\Component\HttpFoundation\StreamedResponse;

trait GeneratesPDF
{
    protected string $pdf_view;

    protected array $pdf_parameters;

    protected $generated_pdf;

    public function getPDFInstance(){
        return $this->pdf;
    }

    public function downloadPDF(){
        $response = $this->getGeneratedPDF();
        response()->streamDownload(fn () => print($response->output()), $this->getFilename());
    }

    public function generatePDF(){
        $pdf = $this->getPDFInstance();
        $this->generatedPDF($pdf::loadView($this->getPDFView(),$this->getPDFParameters()));
    }

    public function generatedPDF($pdf):static 
    {
        $this->generated_pdf = $pdf;
        return $this;
    }

    public function getGeneratedPDF(){
        return $this->generated_pdf;
    }

    public function getPDFParameters():array {
        return $this->pdf_parameters;
    }

    public function pdfParameters(array $parameters):static 
    {
        $this->pdf_parameters = $parameters;
        return $this;
    }

    public function pdfView(string $view ):static{
        $this->pdf_view = $view;
        return $this;
    }

    public function getPDFView():string
    {
        return $this->pdf_view;
    }
}
